package ftoop;

import org.junit.Test;

import student.TestCase;
/**
 * This test class test the ZaehlerDrucker class, thereby testing the interplay of the classes speicher, drucker and zaehler.
 * @author unknown
 *
 */

public class ZaehlerDruckerTest extends TestCase {

	public void testZaehlerDrucker() throws InterruptedException {
		ZaehlerDrucker.main(new String[] { "25" });
		assertFuzzyEquals(
				"0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 ",
				systemOut().getHistory());
	}

}
