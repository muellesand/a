package ftoop;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * This test class test all the methods of the speicher class.
 * @author Melanie Calame
 * @author Sandrine Müller
 * @version 1.o
 */

public class SpeicherTest {
		
		@Test
		public void getAndSetWert() throws InterruptedException {
			Speicher speicher = new Speicher();
			speicher.setWert(2);
			assertEquals(speicher.getWert(), 2);
		}
		
		@Test 
		public void isHatWert() throws InterruptedException {
			Speicher speicher = new Speicher();
			assertEquals(speicher.isHatWert(), false);
			speicher.setWert(2);
			assertEquals(speicher.isHatWert(), true);
			speicher.getWert();
			assertEquals(speicher.isHatWert(), false);
		}
		
		@Test
		public void getFertig() {
			Speicher speicher = new Speicher();
			assertEquals(speicher.getFertig(), false);
			speicher.setFertig(true);
			assertEquals(speicher.getFertig(), true);
		}
		
		@Test 
		public void setFertig() {
			Speicher speicher = new Speicher();
			speicher.setFertig(true);
			assertEquals(speicher.getFertig(), true);
			speicher.setFertig(false);
			assertEquals(speicher.getFertig(), false);
		}

	}
