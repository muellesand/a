package ftoop;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class implements the SpeicherIf interface. It instantiates speicher objects which can store one value provided by a zaehler and retrieved by a drucker thread.
 * @author Melanie Calame
 * @author Sandrine Müller
 * @version 1.0
 */

public class Speicher implements SpeicherIf{

	private int wert;
	private boolean hatWert;
	private boolean fertig;
	
	/**
	 * To instantiate a speicher object, no parameter are needed
	 */
	public Speicher() {
		hatWert = false;
		fertig = false;
	}
	
	/**
	 * This method sets the boolean hatWert to false to signal that the value in the speicher object has been retrieved and then returns the value that is currently
	 * stored in it. 
	 */
	@Override
	public synchronized int getWert() throws InterruptedException {
		//hatWert is set to false to signal that this value has already been retrieved and printed
		this.hatWert = false;
		return wert;
		
	}
	
	/**
	 * This method sets the value stored in the speicher object and sets the boolean hatWert to true to signal that a new value has been provided.
	 */
	@Override
	public synchronized void setWert(int wert) throws InterruptedException {
		this.wert = wert;
		hatWert = true;
	}
	
	/**
	 * This method returns the value of hatWert. True means that there is a value stored in the speicher object that has not yet been retrieved, false
	 * means that the value that is stored in the speicher object has already been retrieved. 
	 */
	@Override
	public synchronized boolean isHatWert() {
		return hatWert;
	}
	
	/**
	 * This method checks what the value of fertig is. 
	 * @return boolean. True means that the counting thread has counted up to the maximum and stored all the values in the speicher object so that no more will
	 * follow which means that the printing thread can kill itself. False means that more values for the printing thread will be provided by the counting thread.
	 */
	public synchronized boolean getFertig() {
		return fertig;
	}
	
	/**
	 * This method sets the value of fertig to either true or false.
	 * @param fertig boolean that fertig should be set to.
	 */
	public synchronized void setFertig(boolean fertig) {
		this.fertig = fertig;
	}
}
