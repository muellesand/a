package ftoop;
/**
 * This class contains the main method where all the other classes are called and coordinated. 
 * @author Melanie Calame
 * @author Sandrine Müller
 * @version 1.0
 *
 */

public class ZaehlerDrucker {
	/**
	 * Main method that instantiates a speicher object and passes it on while instantiating both a drucker and a zaehler object. It then starts the threads
	 * created in zaehler and drucker.
	 * @param args a number needs to be entered that is set as the maximum to which is counted
	 * @throws InterruptedException
	 */

	public static void main(String[] args) throws InterruptedException {
		/*if (args.length != 1) {
			System.out.println("Usage: ZaehlerDrucker <max>");
			System.exit(1);
		}*/

		Speicher s = new Speicher();
		Drucker d = new Drucker(s);
		Zaehler z = new Zaehler(s, Integer.parseInt(args[0]));
		z.start();
		d.start();

		// bissi warten, damit der Test funktioniert
		Thread.sleep(5000);

	}

}
