package ftoop;
/**
 * This class instantiates zaehler objects which count from 0 to a defined number and store these values one by one in a speicher object.
 * @author Melanie Calame
 * @author Sandrine Müller
 * @version 1.0
 *
 */

public class Zaehler extends Thread {

	private Speicher speicher;
	private int max, min;
	// start() is called in the main method
	private Thread countingThread = new Thread();

	/**
	 * Instantiation of a zaehler object
	 * @param s: speicher object that holds the current number.
	 * @param max: End value for the counter (inclusive). 
	 * 
	 */
	public Zaehler(Speicher s, int max) {
		this.speicher = s;
		this.max = max;
		this.min = 0;
	}

	/**
	 * This run method increases the value of the speicher object. it starts at 0 and counts up to max (inclusive).
	 * 
	 */
	@Override
	public void run() {
		// Thread kills itself once it has reached the maximum
		for (int i = this.min; i <= this.max; i++) {
			/* if it is stopped after evaluating whether or not there is a value left in the speicher object, the worst that can
			 * happen is that it thinks there still is a value, so that it cannot write another one and in the meantime the
			 * value is removed by the printer, then it just passes on a writing opportunity but no inconsistencies can happen
			 */
			if (!speicher.isHatWert()) {
				try {
					//synchronized method which also sets hatWert to true
					speicher.setWert(i);
					sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		/*If the thread cannot continue with this statement after having counted to the maximum, the worst that can happen
		 * is that the printing thread checks if there is a value to print one times to many because fertig is not yet set
		 * to true, so it enters the while-loop and checks whether there is a value left, which is not the case after having
		 * removed the last one.
		 */
		speicher.setFertig(true);
	}

}
