package ftoop;
/**
 * Class that instantiates drucker objects which print every value that is stored in the speicher
 * @author Melanie Calame
 * @author Sandrine Müller
 * @version 1.0
 *
 */
public class Drucker extends Thread {
	private Speicher speicher;
	// start() and run() are called in the main method
	private Thread printingThread = new Thread();

	/**
	 * Instantiates drucker object
	 * @param s: speicher object which it prints the values of
	 */
	Drucker(Speicher s) {
		this.speicher = s;
	}

	/**
	 * This run method gets a value from the zaehler via the speicher and prints it followed by a space, then it gets the next value until the
	 * zaehler does not deliver any more values
	 * 
	 */
	@Override
	public synchronized void run() {
		/* Ensures thread kills itself once it has read all the numbers. If it is stopped after checking and in the meantime
		 * the counting thread sets it to true, it enters the loop one times too many but it does not lead to inconsistencies.
		 */
		while (!speicher.getFertig()) {
			/* checks whether hatWert is set to false or true to determine whether the value stored in speicher has already been 
			 * read or not. If it is stopped after checking, the same scenario as when checking for fertig applies.
			 * There is never a problem because only the counting thread can set hatWert to true and only the printing
			 * thread can set it to false by calling the setWert() and getWert() methods, respectively.
			 */
			if (speicher.isHatWert()) {
				try {
					System.out.print(speicher.getWert() + " ");

				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
